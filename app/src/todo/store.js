import {types} from "mobx-state-tree";
import api from "../commons/utils/api";

const Todo = types.model("Todo", {
  title: types.string,
  done: false,
}).actions(self => ({
  toggle() {
    self.done = !self.done
  }
}));

const Store = types.model("Store", {
  todos: types.array(Todo),
  hideComplete: false,
  query: "",
  loading: false
}).views(self => ({
  get count() {
    return self.filteredTodos.length
  },
  get filteredTodos() {
    return self.todos
      .filter(t => self.hideComplete ? !t.done : true)
      .filter(t => self.query ? t.title.includes(self.query) : true);
  },
})).actions(self => ({
  create(values) {
    self.load();
    self.todos.push(values)
  },
  toggleHideComplete() {
    self.load();
    self.hideComplete = !self.hideComplete;
  },
  setQuery(query) {
    self.load();
    self.query = query;
  },
  loaded (todos) {
    self.loading = false;
    self.todos = todos;
  },
  async load() {
    self.loading = true;
    await api.getTodos();
    self.loaded(self.todos)
  }
}));


export default Store;
