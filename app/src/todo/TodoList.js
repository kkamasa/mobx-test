import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Toggle from 'material-ui/Toggle';
import Checkbox from 'material-ui/Checkbox';
import MiTextField from 'material-ui/TextField';
import {Card, CardHeader, CardText} from 'material-ui/Card';
import {Col, Grid, Row} from "react-bootstrap";
import Form from "../commons/form/components/Form";
import TextField from "../commons/form/components/TextField";

class TodoList extends Component {
  rules = {
    title: 'required|min:4',
  };

  componentWillMount () {
    this.props.store.load();
  }

  renderForm (form) {
    return (
      <form onSubmit={async (e) => {
        await form.submitForm(e);
        if (form.getFormState().submitted) {
          form.resetAll()
        }
      }}>
        <TextField field="title" placeholder="Title"/>
        <div>
          <RaisedButton disabled={!!form.errors} type={'submit'} label="Add todo" primary={true}/>
        </div>
      </form>
    )
  }

  render() {
    const {store} = this.props;
    return (
      <Card className={store.loading ? 'loading' : ''}>
        <CardHeader
          title="Todos list"
          actAsExpander={true}
        />
        <CardText>
          <Grid>
            <Row>
              <Col md={6}>
                <Form onSubmit={store.create} render={this.renderForm} rules={this.rules}/>
              </Col>
              <Col md={6}>
                <MiTextField
                  hintText={'Search'}
                  value={store.query}
                  onChange={e => store.setQuery(e.target.value)}
                />
                <Checkbox
                  checked={store.hideComplete}
                  label="Hide complete"
                  onCheck={store.toggleHideComplete}
                />
              </Col>
            </Row>
          </Grid>
        </CardText>
        <CardText>
          <div className={'App-container'}>
            {!store.loading && <List >
              {store.filteredTodos.map((t, i) =>
                <ListItem key={i} primaryText={t.title} rightToggle={
                  <Toggle toggled={t.done} onToggle={t.toggle} />
                } />
              )}
              <ListItem primaryText={"items count: "+store.count} />
            </List>}
          </div>
        </CardText>
      </Card>
    );
  }
}

export default inject("store")(observer(TodoList));
