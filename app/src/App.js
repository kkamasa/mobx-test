import React, {Component} from 'react';
import Container from "./commons/Container";
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import TodoList from "./todo/TodoList";
import Home from "./home/Home";
import {AppBar, Drawer, FlatButton, MenuItem} from "material-ui";

class App extends Component {
  state = {
    sideOpen: false
  };

  toggleSideMenu () {
    console.log('asd');
    this.setState({sideOpen: !this.state.sideOpen})
  }
  hideSideMenu () {
    console.log('asd');
    this.setState({sideOpen: false})
  }

  render() {
    return (
      <Router>
        <div>
          <AppBar
            title="Test app"
            iconElementRight={<FlatButton label="Login" />}
            onLeftIconButtonClick={this.toggleSideMenu.bind(this)}
          />
          <Drawer open={this.state.sideOpen} docked={false} onRequestChange={this.hideSideMenu.bind(this)}>
            {[
              {route: '/', title: 'Home'},
              {route: '/todos', title: 'Todo list'}
            ].map(r =>
              <Link key={r.route} to={r.route}>
                <MenuItem onClick={this.hideSideMenu.bind(this)}>{r.title}</MenuItem>
              </Link>
            )}
          </Drawer>
          <Container className={'mt-1'}>
            <Route exact path="/" component={Home}/>
            <Route exact path="/todos" component={TodoList}/>
          </Container>
        </div>
      </Router>
    );
  }
}

export default App;
