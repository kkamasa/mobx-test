import React from 'react'

export default (props) =>
  <div className={"container "+props.className}>
    {props.children}
  </div>