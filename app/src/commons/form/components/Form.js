import React from 'react';
import {Form} from "react-form";
import createValidator from "../utils/createValidator";

export default (props) =>
  <Form {...props} validate={createValidator(props.rules)}/>;
