import React from 'react';
import {Field} from "react-form";
import {TextField as UiTextField} from "material-ui";

const TextField = (props) =>
  <Field validate={props.validate} field={props.field}>
    {fieldApi => {
      const {onChange, onBlur, field, ...rest} = props;

      const {value, error, warning, success, setValue, setTouched} = fieldApi

      return (
        <UiTextField
          value={value || ''}
          onChange={e => {
            setValue(e.target.value);
            if (onChange) {
              onChange(e.target.value, e)
            }
          }}
          onBlur={e => {
            setTouched();
            if (onBlur) {
              onBlur(e)
            }
          }}
          hintText={props.placeholder}
          errorText={error}
        />
      )
    }}
  </Field>;

export default TextField;