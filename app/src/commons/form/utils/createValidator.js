import Validator from "validatorjs";


export default rules => values => {
  const validation = new Validator(values, rules);
  validation.passes();
  const errors = {};

  Object.keys(rules).map(key => {
    errors[key] = validation.errors.first(key)
  });

  return errors;
}
