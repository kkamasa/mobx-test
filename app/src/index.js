import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import TodoList from "./todo/TodoList";
import registerServiceWorker from './registerServiceWorker';
import Store from "./todo/store";
import { connectReduxDevtools } from "mst-middlewares"
import {Provider} from "mobx-react";
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import './index.css';

const store = Store.create({
  todos: [],
});

connectReduxDevtools(require("remotedev"),store);

ReactDOM.render(
  <MuiThemeProvider>
    <Provider store={store}>
      <App/>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root'));
registerServiceWorker();
