import React from 'react'
import {Link} from "react-router-dom";

export default () =>
  <div>
    Home page
    <div>
      <Link to={'/todos'}>go to todos</Link>
    </div>
  </div>